//
//  CalcControllerViewModel.swift
//  cacluikit
//
//  Created by Paul on 24/07/2023.
//

import Foundation

enum CurrentNumber {
    case firstNumber, secondNumber
}

class CalcControllerViewModel {
    
    var updateViews: ( () -> Void)?
    
    let calcButtonCells: [CalculatorButton] = [
        .aC, .plusMinus, .percentage, .divide,
        .number(7), .number(8), .number(9), .multiply,
        .number(4), .number(5), .number(6), .subtract,
        .number(1), .number(2), .number(3), .add,
        .number(0), .decimal, .equals
    ]
    
    private(set) lazy var calcHeaderLabel: String = firstNumber ?? "0"
    private(set) var currentNumber: CurrentNumber = .firstNumber
    
    private(set) var firstNumber: String? = nil { didSet { self.calcHeaderLabel = self.firstNumber?.description ?? "0" }}
    private(set) var secondNumber: String? = nil { didSet { self.calcHeaderLabel = self.secondNumber?.description ?? "0" }}
    
    private(set) var operation: CalculatorOperation? = nil
    
    private(set) var firstNumberIsDecimal: Bool = false
    private(set) var secondNumberIsDecimal: Bool = false

    
    private(set) var prevNumber: String? = nil
    private(set) var prevOperation: CalculatorOperation? = nil
    
    var eitherNumberIsDecimal: Bool {
        return firstNumberIsDecimal || secondNumberIsDecimal
    }
}

extension CalcControllerViewModel {
    
    public func didSelectButton(with calcButton: CalculatorButton) {
        switch calcButton {
            case .aC: didSelectAllClear()
            case .plusMinus: didSelectPlusMinus()
            case .percentage: didSelectPercentage()
            case .divide: didSelectOperation(with: .divide)
            case .multiply: didSelectOperation(with: .multiply)
            case .subtract: didSelectOperation(with: .subtract)
            case .add: didSelectOperation(with: .add)
            case .equals: didSelectEqualsButton()
            case .number(let number): didSelectNumber(with: number)
            case .decimal: didSelectDecimal()
                
        }
        
        self.updateViews?()
    }
    
    private func didSelectAllClear() {
        calcHeaderLabel = "0"
        currentNumber = .firstNumber
        firstNumber = nil
        secondNumber = nil
        operation = nil
        firstNumberIsDecimal = false
        secondNumberIsDecimal = false
        prevNumber = nil
        prevOperation = nil
    }
}

extension CalcControllerViewModel {
    private func didSelectNumber(with number: Int) {
        
        if currentNumber == .firstNumber {
            
            if var firstNumber = self.firstNumber {
                firstNumber.append(number.description)
                self.firstNumber = firstNumber
                self.prevNumber = firstNumber
            } else {
                self.firstNumber = number.description
                self.prevNumber = number.description
            }
            
        } else {
            
            if var secondNumber = self.secondNumber {
                secondNumber.append(number.description)
                self.secondNumber = secondNumber
                self.prevNumber = secondNumber
                
            } else {
                secondNumber = number.description
                prevNumber = number.description
            }
            
        }
        
    }
}

extension CalcControllerViewModel {
    
    private func didSelectEqualsButton() {
        
        if let operation = self.operation,
           let firstNumber = firstNumber?.toDouble,
           let secondNumber = secondNumber?.toDouble {
            
            let result = getOperationResult(operation, firstNumber, secondNumber)
            let resultString = eitherNumberIsDecimal ? result.description : result.toInt?.description
            
            self.secondNumber = nil
            self.prevOperation = operation
            self.operation = nil
            self.firstNumber =  resultString
            self.currentNumber = .firstNumber
            
        } else if let prevOperation = self.prevOperation,
                  let firstNumber = firstNumber?.toDouble,
                  let prevNumber = prevNumber?.toDouble {
            
            let result = getOperationResult(prevOperation, firstNumber, prevNumber)
            let resultString = eitherNumberIsDecimal ? result.description : result.toInt?.description
            self.firstNumber = resultString
        }
    }
    
    private func didSelectOperation(with operation: CalculatorOperation) {
    
        if currentNumber == .firstNumber {
            self.operation = operation
            currentNumber = .secondNumber
        } else if currentNumber == .secondNumber {
            
            if let prevOperation = self.operation,
               let firstNumber = firstNumber?.toDouble,
               let secondNumber = secondNumber?.toDouble {
                
                let result = getOperationResult(prevOperation, firstNumber, secondNumber)
                let resultString = eitherNumberIsDecimal ? result.description : result.toInt?.description
                
                self.secondNumber = nil
                self.firstNumber = resultString
                currentNumber = .secondNumber
                self.operation = operation
                
            } else {
                self.operation = operation
            }
            
        }
        
    }
    
    private func getOperationResult(_ operation: CalculatorOperation, _ firstNumber: Double?, _ secondNumber: Double?) -> Double {
        guard let firstNumber = firstNumber, let secondNumber = secondNumber else { return 0 }
        
        switch operation {
            case .divide:
                return (firstNumber / secondNumber)
            case .multiply:
                return (firstNumber * secondNumber)
            case .subtract:
                return (firstNumber - secondNumber)
            case .add:
                return (firstNumber + secondNumber)
        }
    }
    
}

extension CalcControllerViewModel {
    
    private func didSelectPlusMinus() {
        if currentNumber == .firstNumber, var number = firstNumber {
            
            if number.contains("-") {
                number.removeFirst()
            } else {
                number.insert("-", at: number.startIndex)
            }
            
            firstNumber = number
            prevNumber = number
            
        } else if currentNumber == .secondNumber, var number = secondNumber {
            
            if number.contains("-") {
                number.removeFirst()
            } else {
                number.insert("-", at: number.startIndex)
            }
            
            secondNumber = number
            prevNumber = number
        }
    }
    
    private func didSelectPercentage() {
        if currentNumber == .firstNumber, var number = firstNumber?.toDouble {
            
            number /= 100
            
            if number.isInteger {
                firstNumber = number.toInt?.description
            } else {
                firstNumber = number.description
                firstNumberIsDecimal = true
            }
            
        } else if currentNumber == .secondNumber, var number = secondNumber?.toDouble {
            
            number /= 100
            
            if number.isInteger {
                secondNumber = number.toInt?.description
            } else {
                secondNumber = number.description
                secondNumberIsDecimal = true
            }
            
        }
    }
    
    private func didSelectDecimal() {
        if currentNumber == .firstNumber {
            firstNumberIsDecimal = true
            
            if let firstNumber = self.firstNumber, !firstNumber.contains(".") {
                self.firstNumber = firstNumber.appending(".")
            } else if firstNumber == nil {
                firstNumber = "0."
            }
            
        } else if currentNumber == .secondNumber {
            secondNumberIsDecimal = true
            
            if let secondNumber = self.secondNumber, !secondNumber.contains(".") {
                self.secondNumber = secondNumber.appending(".")
            } else if secondNumber == nil {
                secondNumber = "0."
            }
        }
        
    }
}
