//
//  ButtonCell.swift
//  cacluikit
//
//  Created by Paul on 24/07/2023.
//

import UIKit

class ButtonCell: UICollectionViewCell {
    
    static let identifier = "ButtonCell"
    
    private(set) var calculatorButton: CalculatorButton!
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 40, weight: .regular)
        label.text = "Error"
        return label
    } ()
    
    public func configure(with calculatorButton: CalculatorButton) {
        self.calculatorButton = calculatorButton
        
        self.titleLabel.text = calculatorButton.title
        self.backgroundColor = calculatorButton.color
        
        switch calculatorButton {
            case .aC, .plusMinus, .percentage:
                self.titleLabel.textColor = .black
            default:
                self.titleLabel.textColor = .white
        }
        
        self.setupUI()
    }
    
    public func setOperationSelected() {
        titleLabel.textColor = .orange
        backgroundColor = .white
    }
    
    private func setupUI() {
        
        addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        switch calculatorButton {
            case let .number(int) where int == 0:
                
                self.layer.cornerRadius = 36
                
                let extraSpace = frame.size.width - frame.size.height
                
                NSLayoutConstraint.activate([
                    self.titleLabel.heightAnchor.constraint(equalToConstant: frame.height),
                    self.titleLabel.widthAnchor.constraint(equalToConstant: frame.height),
                    self.titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
                    self.titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
                    self.titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor,
                                                             constant: -extraSpace)
                ])
                
            default:
               layer.cornerRadius = frame.size.width / 2
                NSLayoutConstraint.activate([
                    self.titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
                    self.titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
                    self.titleLabel.heightAnchor.constraint(equalTo: heightAnchor),
                    self.titleLabel.widthAnchor.constraint(equalTo: widthAnchor)
                ])
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.titleLabel.removeFromSuperview()
    }
}
